<!doctype html>
<html class="no-js " lang="en"> <!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/misc/fav.jpg">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>GRAPHIC TELEMATION-Official Website</title>
  <!-- Bootstrap Core CSS-->
  <link href="<?php echo site_url()?>css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS-->
  <link href="<?php echo site_url()?>css/universal.css" rel="stylesheet">
  <link href="<?php echo site_url()?>css/jaabs.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300" rel="stylesheet">
</head>
<body>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
  
    <nav class="navbar navbar-universal navbar-custom navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#page-top" class="navbar-brand page-scroll">
            <!-- Text or Image logo--><!-- <img src="img/logo.png" alt="Logo" class="logo"> --><img src="img/logodark.png" alt="Logo" class="logodark"></a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
         <ul class="nav navbar-nav navbar-left">
            <li class="hidden"><a href="#page-top"></a></li>
            <li class="hidden"><a href="#page-top"></a></li>
            <li><a href="<?php echo site_url('/'); ?>">HOME </a></li>
            <li><a href="<?php echo site_url('pages/aboutus'); ?>">ABOUT US  </a></li>
            <li><a href="<?php echo site_url('pages/corporate'); ?>">CORPORATE <span class="caret"></span></a> </li>
            <li><a href="<?php echo site_url('pages/adfilims'); ?>">AD FILMS <span class="caret"></span></a></li>
            <li><a href="<?php echo site_url('pages/Albums'); ?>">GALLERY <span class="caret"></span></a></li>
            <li><a href="#">SERVICES <span class="caret"></span></a>
            <ul class="dropdown-menu columns-1">
                <li><a href="<?php echo site_url('pages/photography'); ?>">Photography</a></li>
                <li><a href="<?php echo site_url('pages/Production'); ?>">Production</a></li>
                <li><a href="<?php echo site_url('pages/Location'); ?>">Location</a></li>
                <li><a href="<?php echo site_url('pages/ConceptAndDesign'); ?>">Concept And Design</a></li>
                <li><a href="#">Marketing Solution</a></li>
            </ul>
            </li>
           
          </ul>
        </div>
      </div>
    </nav>

     <div id="spertaor-logo">
      <div class="col-md-12">

        <div class="col-md-4 logo-div">  
            <img src="img/logo.png" alt="Logo" class="logo logo-img" height="230px"> 
        </div>
        <div class="col-md-8 text-div">
          <div>
            <span>jaabsfilms.com</span>
          </div>
          
        </div>
      </div>
      
     </div>
    <header data-background="img/header/bg-video.jpg" class="intro">
      <!-- Intro Header-->
   
     
      <div class="intro-body">
      
     <!--    <div data-wow-delay="1s" class="scroll-btn wow fadeInDown"><a href="#about" class="page-scroll"><span class="mouse"><span class="weel"><span></span></span></span></a></div> -->
      </div>
      <div class="video-controls video-controls-visible hidden-sm"><a id="video-volume" href="#" class="fa fa-lg fa-volume-up"></a><a id="video-play" href="#" class="fa fa-pause fa-lg"></a></div>
    </header>

    <section id="about" class="section-small">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h3>Who We Are</h3>
            <p class="no-pad">Graphic Telemation is a company that prides itself on 25 years' of experience, service and most modern technologies to meet new generation application needs in audio visual production.</p>

            <p class="no-pad">Graphic Telemation works with highly reputed talents from all over the world to meet high production qualities and values. A full-service advertising solution with its in-house filming equipment company named MOOVEETOOLS.com and production facilities from conceptualisation, design, audio visual production and to application with its impeccable service, state-of-the- art equipment and talented creative force. </p>
            <h2 class="classic">Jabbar Kallarackal</h2>
          </div>
          <div data-wow-duration="2s" data-wow-delay=".2s" class="col-lg-5 col-lg-offset-1 wow zoomIn">
            <div id="carousel-light2" class="carousel slide carousel-fade">
              <ol class="carousel-indicators">
                <li data-target="#carousel-light2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-light2" data-slide-to="1"></li>
                <li data-target="#carousel-light2" data-slide-to="2"></li>
              </ol>
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="img/misc/6.png" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="img/misc/5.png" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="img/misc/4.png" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- About Section-->

      <!-- Services Section-->
    <section id="services" class="bg-img4 text-center">
      <div class="overlay"></div>
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Our Services</h3>
            <p>Graphic Telemation is a one stop advertising and marketing solution provider with its  multitalented creative force and wide experienced technical team both from in-house and

out-sourced.</p>
          </div>
        </div>
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-analytics-outline"></i>Conceptualisation </h4>
            
          </div>
          <div data-wow-delay=".4s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-analytics-outline"></i> Creative ideas</h4>
            
          </div>
          <div data-wow-delay=".6s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-analytics-outline"></i> Design and publication</h4>
            
          </div>
        </div>
         <div class="row">
          <div data-wow-delay=".2s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-analytics-outline"></i>Audio Visual production and screening </h4>
           
          </div>
          <div data-wow-delay=".4s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-analytics-outline"></i> Web Marketing and promotion</h4>
            
          </div>
          <div data-wow-delay=".6s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-analytics-outline"></i> Customer survey supports</h4>
           
          </div>
          
          
        </div>
      </div>
    </section>

    <!-- lATEST Videos-->

     <section id="news">
      <div class="container">
        <h3 class="pull-left">Our Partners</h3>
        <div class="pull-right">
         
        </div>
        <div class="clearfix"></div>
        <div class="row grid-pad">
          <div class="col-sm-6"><a href="#"><img src="<?php echo site_url()?>img/partner/1.jpg" alt="" width="400px" height="200px" class="img-responsive center-block">
              <h5>Voyage Media</h5></a>
            <p>We are a media production and buying agency that creates airtime sales strategies for advertisers in the ethnic TV sector.
 
We work closely with many of the South Asian ethnic TV channels in the UK to place the right brands onto the right broadcast channels.
 
We focus on the Tamil Ethnic Community in Europe.</p>
<p>We work with STAR Vijay International for Adsales of the channel for the Europe Region.</p>

          </div>
          <div class="col-sm-6"><a href="#"><img src="<?php echo site_url()?>img/partner/2.png" alt=""  width="400px" height="200px" class="img-responsive center-block">
              <h5>MindWind</h5></a>
            <p>MindWind Productions is a film production and Advertising agency based in Mauritius, run by people who love making films. We make films for all, be it a Brand, Corporate Organization, or a YouTuber. We do not want to be known as just another Advertising Agency in Mauritius or  Film Production House in Mauritius, but as a team of creative film makers who can bring out the imagination from mind to reality.</p>
            <p>Our aim is to be the one stop where you get all the services of a Film Production House, Advertising Agency and Creative Communications in Mauritius.</p>

          </div>
        </div>
      </div>
    </section>

    <section class="bg-dark wow fadeIn animated">
      <div class="container">
        <div class="row">
          <div data-wow-delay=".2s" class="col-md-5 wow fadeIn">
           <br/>
           <br/>
           <br/>
            <h1>Showreel </h1>
          </div>
          <div data-wow-delay=".4s" class="col-md-6 col-md-offset-1 wow fadeIn">
            <div id="carousel-dark" data-ride="carousel" class="carousel slide carousel-fade">
              <ol class="carousel-indicators">
                <li data-target="#carousel-dark" data-slide-to="0" class="active"></li>
                <!-- <li data-target="#carousel-dark" data-slide-to="1"></li>
                <li data-target="#carousel-dark" data-slide-to="2"></li> -->
              </ol>
              <div role="listbox" class="carousel-inner">
                
                <div class="item active embed-responsive embed-responsive-16by9">
                  <iframe src="https://player.vimeo.com/video/<?php echo $all_videos->VideoUrl; ?>?color=1c58ed&amp;title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="600" height="338" allowfullscreen="" class="embed-responsive-item"></iframe>
                </div>
                <?php

                ?>  
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    

     <section id="news">
      <div class="container">
        <h3 class="pull-left">Our Sister Concern</h3>
        <div class="pull-right">
         
        </div>
        <div class="clearfix"></div>
        <div class="row grid-pad">
          <div class="col-sm-6"><a href="http://mooveetools.com/" target="_blank"><img src="<?php echo site_url()?>img/company/1.jpg" alt="" width="400px" height="200px" class="img-responsive center-block">
              <h5>Mooveetools</h5></a>
            <p>Mooveetools supports projects of all sizes, budgets and styles, from commercials to feature to documentary. Our      well trained crew with advanced cinema tools designed to provide all the cinema rental equipment you need   under one roof.</p>
              <p>Total cinema production solution in South India
              Equipment rental, Crew, talents, locations and logistics.</p>

          </div>
          <!--  <div class="col-sm-6"><a href="http://mooveetools.com/" target="_blank"><img src="<?php echo site_url()?>img/company/1.jpg" alt="" width="400px" height="200px" class="img-responsive center-block">
              <h5>Mooveetools</h5></a>
            <p>Mooveetools supports projects of all sizes, budgets and styles, from commercials to feature to documentary. Our      well trained crew with advanced cinema tools designed to provide all the cinema rental equipment you need   under one roof.</p>
              <p>Total cinema production solution in South India
              Equipment rental, Crew, talents, locations and logistics.</p>

          </div> -->
        </div>
      </div>
    </section>

    <!-- Facts section-->
    <section class="facts section-small bg-img">
      <div class="overlay"></div>
      <div class="container text-center">
        <div class="row">
            <?php 
              foreach ($all_achievement as $achievement) { 
            ?>
              <div class="col-sm-3"><span data-min="0" data-max="<?php echo $achievement['AchievementCount'];  ?>" data-delay="5" data-increment="1" class="numscroller">0</span>
                <h5 class="no-pad"><?php echo $achievement['AchievementName'];  ?></h5>
              </div>
            <?php
              }
            ?>
         
      </div>
    </section>

<!-- Contact Section-->
    <section id="contact" id="section_contact">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <h3>contact us</h3>
            <p>Feel free to contact us.</p>
            <hr>
            <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> Graphic Telemation,A1 Jewel Planet,Vyttila, Ernakulam
            </h5>
            <h5><i class="fa fa-envelope fa-fw fa-lg"></i> info@jaabsfilms.com
            </h5>
            <h5><i class="fa fa-phone fa-fw fa-lg"></i> (0484) 402-0206
            </h5>
          </div>
          <div class="col-md-5 col-md-offset-2">
            <h3>Say hello</h3>
            <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
            <form id="contactForm" name="sentMessage" novalidate="">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="name" class="sr-only control-label">You Name</label>
                  <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg"><span class="help-block text-danger"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="email" class="sr-only control-label">You Email</label>
                  <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email" class="form-control input-lg"><span class="help-block text-danger"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="phone" class="sr-only control-label">You Phone</label>
                  <input id="phone" type="tel" placeholder="You Phone" required="" data-validation-required-message="Please enter phone number" class="form-control input-lg"><span class="help-block text-danger"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="message" class="sr-only control-label">Message</label>
                  <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message." aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
                </div>
              </div>
              <div id="success"></div>
              <button type="submit" class="btn btn-dark">Send</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- Map Section-->
    <div id="map"></div>


    <!-- Footer Section-->
    <section class="section-small footer bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h5>About</h5>
            <p>GRAPHIC TELEMATION and PURPLE DREAMS ..are headed by Jabbar Kallarackal, a young talented cinematographer turned director (Holding Independent Film Makers certificate from THE HOLLYWOOD FILM INSTITUTE. Los Angels for Cinema Director and Line Producer. And Producer’s Diploma from THE HOLLYWOOD FILM INSTITUTE. Los angels) entered in the field of film making in 1989. Steadily growing</p>
          </div>
          <div class="col-sm-2 col-sm-offset-1 footer-menu">
            <h5>Company</h5>
            <h6 class="no-pad"><a href="<?php echo site_url('pages/aboutus'); ?>">ABOUT US  </a></h6>
            <h6 class="no-pad"><a href="<?php echo site_url('pages/corporate'); ?>">CORPORATE </a> </h6>
            <h6 class="no-pad"><a href="<?php echo site_url('pages/adfilims'); ?>">AD FILMS </a></h6>
            <h6 class="no-pad"><a href="<?php echo site_url('pages/Albums'); ?>">GALLERY </a></h6>
            
          </div>
          
          <div class="col-sm-3 text-right" id="div_vistcount">
            
          </div>
        </div>
      </div>
    </section>
    <section class="section-small footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h6>Powered By <a href="http://minusbugs.com/">MinusBugs</a>
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <h6>We <i class="fa fa-heart fa-fw"></i> creative people
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1 text-right">
            <ul class="list-inline">
              <li><a href="#"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- jQuery-->
    <script src="<?php echo site_url()?>js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="<?php echo site_url()?>js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript-->
    <script src="<?php echo site_url()?>js/jquery.easing.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.countdown.min.js"></script>
    <script src="<?php echo site_url()?>js/device.min.js"></script>
    <script src="<?php echo site_url()?>js/form.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.placeholder.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.shuffle.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.parallax.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.circle-progress.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.swipebox.min.js"></script>
    <script src="<?php echo site_url()?>js/smoothscroll.min.js"></script>
    <script src="<?php echo site_url()?>js/wow.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.smartmenus.js"></script>
        <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8FurD9jbl3ybIMWPcPf20pRr7p2vP6fA"></script>
        <script src="<?php echo site_url()?>js/map.js"></script>
    <!-- Youtube video background-->
    <a id="bgndVideo" data-property="{videoURL:'https://www.youtube.com/watch?v=0pM2IK4mnqE', containment:'.intro', autoPlay:true, loop:true, mute:false, startAt:0, stopAt: 100, quality:'default', opacity:1, showControls: false, showYTLogo:false, vol:25}" class="player"></a>
    <script src="<?php echo site_url()?>js/jquery.mb.YTPlayer.js"></script>
    <!-- Custom Theme JavaScript-->
    <script src="<?php echo site_url()?>js/universal.js"></script>

    <script type="text/javascript" src="<?php echo site_url()?>js/jaabs.js"></script>

  </body>

</html>