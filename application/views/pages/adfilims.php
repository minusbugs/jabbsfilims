<header data-background="<?php echo site_url()?>img/header/112.jpeg" class="intro introhalf">
    <!-- Intro Header-->
    <div class="intro-body">
      <h1><?php echo $page_name; ?></h1>
      <h5>Home / <?php echo $page_name; ?></h5>
    </div>
</header>
<div class="section section-small bg-gray">
    <div class="container-fluid">
      <div class="row">
        <?php 
          foreach ($all_videos as $vid_url) { 
        ?>
          <div class="col-md-6">
                  <h5></h5>
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php echo $vid_url['VideoUrl']; ?>?byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                  </div>
          </div>
          
          <?php } 
          ?>
      </div>
     
    </div>
</div>



