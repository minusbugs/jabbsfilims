
  <section id="portfolio" class="portfolio-wide">
   
    <div class="container text-center">
        <div class="row"> 
          <div class="col-sm-10 col-sm-offset-1">
            <h3><?php echo $page_name;?></h3>
          </div>
        </div>
      </div>

    <div class="container-fluid">
        <div id="grid" class="row portfolio-items">
        <?php 

          foreach ($all_gallery as $img) { 
        ?>
           <div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-6">
            <div class="portfolio-item"><a href="#"><img src="<?php echo admin_image_url.$img["GalleryUrl"]; ?>" alt="<?php echo $img["GalleryName"]; ?> ">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5><?php echo $img["GalleryName"]; ?></h5><span><?php echo $img["GalleryDescription"]; ?></span>
                  </div>
                </div></a></div>
          </div>
        <?php 
          }
        ?>
         
        </div>
          
    </div>
  </section>



