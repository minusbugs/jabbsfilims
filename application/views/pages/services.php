<div class="small-header bg-img4">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3><?php echo $page_name; ?></h3>
          </div>
          <div class="col-md-6 text-right">
            <h6 class="breadcrumb"><a href="<?php echo site_url('/'); ?>">Home</a> /<?php echo $title_name; ?>
            </h6>
          </div>
        </div>
      </div>
    </div>
<div class="section section-small bg-gray">
    <div class="container-fluid">
      <div class="row">
        <?php 
          foreach ($all_videos as $vid_url) { 
        ?>
          <div class="col-md-6">
                  <h5></h5>
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php echo $vid_url['VideoUrl']; ?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                  </div>
          </div>
          
          <?php } 
          ?>
      </div>
     
    </div>
</div>



