
  <section>
      <div class="container">
        <div class="row wow fadeIn">
           <div class="col-md-6" >
            <img src="<?php echo site_url()?>img/jaabs/logo.jpg" width="500" height="350">
          </div>
          <div class="col-md-6">
            <h3>The Logo</h3>
              <p>A fast moving, multi-disciplinary, global production house; that's what Graphic Telemation is. The logo captures our worldwide vision and capabilities, to connect and complete any kind of advertising production, efficiently and successfully. </p><br>
        <p>Design Philosophy is contemporary, sleek, global and dynamic.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h3>The Name</h3>
            <p><strong>GRAPHIC TELEMATION</strong>– A fusion derived from three aspects of advertising world.</p>
            <p>GRAPHIC designs evolved from innovative concept and ideas. </p>
            <p>TELE represents the internet and television world, and;</p>
            <p>MATION comes from the word MOTION picture which represents TV commercials, Corporate Films and Features.</p>
            <p>GRAPHIC + TELE + MOTION = <strong>GRAPHIC TELEMATION</strong></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h3>The Storyline </h3>
            <p><strong>Take a company that prides itself of 25years of experience and service. Add cutting-edge technologies to meet new generation application-needs in Audio Visual production. And thus born Graphic Telemation. </strong></p>
            <p>What we have created is a full-service advertising solution with an in-house filming equipment company named MOOVEETOOLS.com and full production facilities from conceptualisation, design, audio visual production (and to application). <strong>We have mastered impeccable service with state-of-the-art equipments by a well talented creative force. We work with the most reputed talents from all over the world to meet and assure the best-in-class production qualities and values we always stick for.</strong></p>
            <br>
            <p><strong>Graphic Telemation manages this whole-spectrum advertising using a strong association with world’s leading model coordinators, technicians and technology providers who can be appropriately engaged to meet the client’s advertising need and to generate customized and creative print, audio-visual, tag lines, catch lines, and write good copy.</strong></p>
            <br>
            <p><strong>HQ is Kochi, India and is a partner venture with Voyage Media, London and Mind Wind Productions, Mauritius.</strong></p>
          </div>
        </div>
      </div>
  </section>