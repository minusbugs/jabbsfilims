 <header data-background="<?php echo site_url()?>img/header/111.jpg" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Gallery</h1>
        <h5>Home / Gallery</h5>
      </div>
 </header>

    <section id="portfolio" class="portfolio-wide">
   
   <div class="container-fluid">
        <div id="grid" class="row portfolio-items">
        <?php 

          foreach ($all_albums as $img) { 

        ?>
            <div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-3">
              <div class="portfolio-item"><a href="<?php echo site_url()?>pages/gallery/<?php echo $img["AlbumId"]; ?>/<?php echo $img["AlbumName"]; ?>"><img src="<?php echo admin_image_url.$img["CoverImage"]; ?>" alt="<?php echo $img["AlbumName"]; ?> ">
                <div class="portfolio-overlay">
                  <div class="caption">
                      <h5><?php echo $img["AlbumName"]; ?></h5><span></span>
                  </div>
                </div></a>
              </div>
            </div>
        <?php 
          }
        ?>
         
        </div>
          
    </div>
    </section>




