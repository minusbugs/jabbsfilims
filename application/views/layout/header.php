<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/misc/fav.jpg">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Jaabs Films</title>
    <!-- Bootstrap Core CSS-->
    <link href="<?php echo site_url()?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS-->
    <link href="<?php echo site_url()?>css/universal.css" rel="stylesheet">
  </head>
  <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
    <!-- Preloader-->
    <!-- <div id="preloader">
      <div id="status"></div>
    </div> -->