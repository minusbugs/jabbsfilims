<!doctype html>
<html class="no-js " lang="en"> <!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/misc/fav.jpg">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>GRAPHIC TELEMATION-Official Website</title>
  <!-- Bootstrap Core CSS-->
  <link href="<?php echo site_url()?>css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS-->
  <link href="<?php echo site_url()?>css/universal.css" rel="stylesheet">
  <link href="<?php echo site_url()?>css/jaabs.css" rel="stylesheet">
</head>
<body>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
   
    <!-- Navigation-->
    <nav class="navbar navbar-custom navbar-fixed-top top-nav-collapse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#page-top" class="navbar-brand page-scroll">
            <!-- Text or Image logo--><img src="<?php echo site_url()?>img/logo.png" alt="Logo" class="logo"><img src="<?php echo site_url()?>img/logodark.png" alt="Logo" class="logodark"></a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
        <ul class="nav navbar-nav navbar-left">
            <li class="hidden"><a href="#page-top"></a></li>
            <li class="hidden"><a href="#page-top"></a></li>
            <li><a href="<?php echo site_url('/'); ?>">HOME </a></li>
            <li><a href="<?php echo site_url('pages/aboutus'); ?>">ABOUT US  </a></li>
            <li><a href="<?php echo site_url('pages/corporate'); ?>">CORPORATE <span class="caret"></span></a> </li>
            <li><a href="<?php echo site_url('pages/adfilims'); ?>">AD FILMS <span class="caret"></span></a></li>
            <li><a href="<?php echo site_url('pages/Albums'); ?>">GALLERY <span class="caret"></span></a></li>
            <li><a href="#">SERVICES <span class="caret"></span></a>
            <ul class="dropdown-menu columns-1">
                <li><a href="<?php echo site_url('pages/photography'); ?>">Photography</a></li>
                <li><a href="<?php echo site_url('pages/Production'); ?>">Production</a></li>
                <li><a href="<?php echo site_url('pages/Location'); ?>">Location</a></li>
                <li><a href="<?php echo site_url('pages/ConceptAndDesign'); ?>">Concept And Design</a></li>
                <li><a href="#">Marketing Solution</a></li>
            </ul>
            </li>
           
          </ul>
        </div>
      </div>
    </nav>
    <?php $this->load->view($content); ?>

  <section class="section-small footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
        <h6>Powered By <a href="http://minusbugs.com/">MinusBugs</a>
        </h6>
        </div>
        <div class="col-sm-3 col-sm-offset-1">
          <h6>We <i class="fa fa-heart fa-fw"></i> creative people
          </h6>
        </div>
        <div class="col-sm-3 col-sm-offset-1 text-right">
          <ul class="list-inline">
            <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
            <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
            <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
            <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
    <!-- jQuery-->
    <script src="<?php echo site_url()?>js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="<?php echo site_url()?>js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript-->
    <script src="<?php echo site_url()?>js/jquery.easing.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.countdown.min.js"></script>
    <script src="<?php echo site_url()?>js/device.min.js"></script>
    <script src="<?php echo site_url()?>js/form.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.placeholder.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.shuffle.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.parallax.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.circle-progress.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.swipebox.min.js"></script>
    <script src="<?php echo site_url()?>js/smoothscroll.min.js"></script>
    <script src="<?php echo site_url()?>js/wow.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.smartmenus.js"></script>
    <!-- Custom Theme JavaScript-->
    <script src="<?php echo site_url()?>js/universal.js"></script>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </body>
</html>