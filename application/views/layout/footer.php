
    <!-- jQuery-->
    <script src="<?php echo site_url()?>js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="<?php echo site_url()?>js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript-->
    <script src="<?php echo site_url()?>js/jquery.easing.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.countdown.min.js"></script>
    <script src="<?php echo site_url()?>js/device.min.js"></script>
    <script src="<?php echo site_url()?>js/form.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.placeholder.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.shuffle.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.parallax.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.circle-progress.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.swipebox.min.js"></script>
    <script src="<?php echo site_url()?>js/smoothscroll.min.js"></script>
    <script src="<?php echo site_url()?>js/wow.min.js"></script>
    <script src="<?php echo site_url()?>js/jquery.smartmenus.js"></script>
        <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8FurD9jbl3ybIMWPcPf20pRr7p2vP6fA"></script>
        <script src="<?php echo site_url()?>js/map.js"></script>
    <!-- Youtube video background-->
    <!-- <a id="bgndVideo" data-property="{videoURL:'https://www.youtube.com/watch?v=0pM2IK4mnqE', containment:'.intro', autoPlay:true, loop:true, mute:false, startAt:0, stopAt: 100, quality:'default', opacity:1, showControls: false, showYTLogo:false, vol:25}" class="player"></a> -->
    <script src="<?php echo site_url()?>js/jquery.mb.YTPlayer.js"></script>
    <!-- Custom Theme JavaScript-->
    <script src="<?php echo site_url()?>js/universal.js"></script>

    <script type="text/javascript" src="<?php echo site_url()?>js/jaabs.js"></script>

  </body>

</html>