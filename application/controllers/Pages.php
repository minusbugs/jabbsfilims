<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
    
     public function __construct() {
    //load database in autoload libraries 
        parent::__construct(); 
        $this->load->helper('url_helper');
        $this->load->helper('form'); 
        $this->load->helper('file');
        $this->load->model('VideosModel');    
        $this->load->model('GalleryModel');       
        $this->load->model('AlbumModel');  
        $this->load->model('AchievementModel');

   }
    public function index()
    {
        $data['main_title']="GRAPHIC TELEMATION and PURPLE DREAMS";
        $data['title_name']="home";
        $data['page_name']="Home";
        $data['all_videos']=$this->VideosModel->get_all_videos_by_id(6);
        $data['all_achievement']=$this->AchievementModel->get_all_achievement();
        

        $this->count_visitor();
        $this->load->view('pages/home_page',$data);
    
    }

    public function count_visitor(){
        $file_content = read_file('./contents/vist_count.txt');
        $file_content=$file_content+1;
        write_file('./contents/vist_count.txt', $file_content);
    }   

    public function corporate()
    {
        $data = array('content'=>'pages/corporate',
        'all_videos'=>$this->VideosModel->get_all_videos_by_typeid(2),
        'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS-Corporate ',
        'title_name'=>'Corporate',
        'page_name'=>'Corporate Videos',
        );
        $this->load->view('layout/template',$data);
       
    }
     public function adfilims()
    {
        $data = array('content'=>'pages/adfilims',
        'all_videos'=>$this->VideosModel->get_all_videos_by_typeid(1),
        'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS-Corporate ',
        'title_name'=>'Ad Filims',
        'page_name'=>'Ad Filims',
        );
        $this->load->view('layout/template',$data);
       
    }
    public function photography()
    {
        $data = array('content'=>'pages/photography',
        'all_gallery'=>$this->GalleryModel->get_all_gallery_by_typeid(4),
        'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS ',
        'title_name'=>'Photography',
        'page_name'=>'Photography',
        );
        $this->load->view('layout/template',$data);
       
    }
    public function Albums(){
       // print_r($this->AlbumModel->get_all_album());exit;
          $data = array('content'=>'pages/AlbumPage',
            'all_albums'=>$this->AlbumModel->get_all_album(),
            'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS ',
            'title_name'=>'Photo Gallery',
            'page_name'=>'Gallery',
            );
        $this->load->view('layout/template',$data);
    }
    public function gallery($id,$albumName)
    {
        $data = array('content'=>'pages/gallery',
        'all_gallery'=>$this->AlbumModel->get_all_gallery_by_albumid($id),
        'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS ',
        'title_name'=>$albumName,
        'page_name'=>$albumName,
        );
        $this->load->view('layout/template',$data);
       
    }
     public function aboutus()
    {
        $data = array('content'=>'pages/aboutus',
        'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS-About Page ',
        'title_name'=>'About Us',
        'page_name'=>'About Us',
        );
        $this->load->view('layout/template',$data);
       
    }

    public function ConceptAndDesign(){
         $data = array('content'=>'pages/ConceptAndDesign',
            'all_gallery'=>$this->GalleryModel->get_all_gallery_by_typeid(5),
            'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS-Concept And Design ',
            'title_name'=>'Concept And Design',
            'page_name'=>'Concept And Design',
        );
        $this->load->view('layout/template',$data);
    }
    public function Production(){
         $data = array('content'=>'pages/Production',
            'all_gallery'=>$this->GalleryModel->get_all_gallery_by_typeid(6),
            'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS-Production ',
            'title_name'=>'Production',
            'page_name'=>'Production',
        );
        $this->load->view('layout/template',$data);
    }
    
    public function Location(){
         $data = array('content'=>'pages/Location',
            'all_gallery'=>$this->GalleryModel->get_all_gallery_by_typeid(7),
            'main_title'=>'GRAPHIC TELEMATION and PURPLE DREAMS-Location ',
            'title_name'=>'Location',
            'page_name'=>'Location',
        );
        $this->load->view('layout/template',$data);
    }
    
    public function hai(){
        echo "haiii";
    }
}
