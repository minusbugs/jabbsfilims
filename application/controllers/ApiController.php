<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiController extends CI_Controller {
    

	 public function __construct() {
    //load database in autoload libraries 
      	parent::__construct(); 
		$this->load->helper('url_helper');
      	$this->load->model('VideosModel');         
   }

	public function v1_getallvideios(){
		$data =  $this->VideosModel->get_all_videos();
		echo json_encode($data);

	}

}

?>