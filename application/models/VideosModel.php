<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VideosModel extends CI_Model{
    var $table = 'videos';
    function __construct()
    {
       $this->load->database();
    }

    public function get_all_videos(){
        
        $this->db->select('*');
        $this->db->from('videos');
        $this->db->order_by('videos.VideoId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }

    public function get_all_videos_by_typeid($TypeId){
        
        $this->db->select('*');
        $this->db->from('videos');
        $this->db->where('AttachmentId',$TypeId);
        $this->db->order_by('videos.VideoId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }
      public function get_all_videos_by_id($VideoId){
        
        $this->db->select('*');
        $this->db->from('videos');
        $this->db->where('VideoId',$VideoId);
        $this->db->order_by('videos.VideoId','desc');
        $query = $this->db->get();

        return $query->row();
       
    }
}

?>