<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryModel extends CI_Model{
    var $table = 'gallery';
    function __construct()
    {
       $this->load->database();
    }

    public function get_all_gallery(){
        
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->order_by('gallery.GalleryId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }
    public function get_all_gallery_by_typeid($TypeId){
        
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->where('AttachmentId',$TypeId);
        $this->db->order_by('gallery.GalleryId','desc');
        $query = $this->db->get();
       
        return $query->result_array();
       
    }
}

?>