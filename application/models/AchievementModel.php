<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AchievementModel extends CI_Model{
    var $table = 'achievement';
    function __construct()
    {
       $this->load->database();
    }

    public function get_all_achievement(){
        
        $this->db->select('*');
        $this->db->from('achievement');
        $this->db->order_by('achievement.AchievementId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }
    
}

?>