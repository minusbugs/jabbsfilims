<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlbumModel extends CI_Model{
    var $table = 'album';
    function __construct()
    {
       $this->load->database();
    }

    public function get_all_album(){
        
        $this->db->select('*');
        $this->db->from('album');
        $this->db->order_by('album.AlbumId','desc');
        $query = $this->db->get();

        return $query->result_array();
       
    }
    public function get_all_gallery_by_albumid($AlbumId){
        
        $this->db->select('*');
        $this->db->from('gallery');
        $this->db->where('AlbumId',$AlbumId);
        $this->db->order_by('gallery.GalleryId','desc');
        $query = $this->db->get();
       
        return $query->result_array();
       
    }
}

?>